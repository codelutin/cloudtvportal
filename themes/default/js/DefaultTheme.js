/*
 * Cloud TV Portal, (C) 2015 Code Lutin (SAS).
 * Copyright (C) <year>  <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import commons from "../style/shared/commons.json";
import list from "../style/shared/list.json";

import React from "react";

export class DefaultTheme {
    createSource(text, image) {
        return (
            <div className="theme-root-item" >
                <span>{text}</span>
                <img src={image || "//:0"}/>
            </div>
        );
    }

    createChild(text, image, style = {}, options = {}) {
        style.height = style.height || list.defaultItemHeight;

        let img;
        if (image) {
            img = <img className={"theme-child-item-icon " + (options.isLarge ? "isLarge" : "") + (options.isSmall ? "isSmall" : "")} src={image || "//:0"}/>;
        }

        return (
            <div className={"theme-child-item " + (image ? "theme-child-item--withIcon" : "")} style={style}>
                {img}
                <div className={"theme-child-item-text " + (image ? "theme-child-item-text--withIcon" : "")}>{text}</div>
            </div>
        );
    }

    createSimpleContent(text, image, style) {
        return (
            <div className="theme-content" style={style}>
                <img className={"theme-content-icon " + (!image ? "hidden" : "")} src={image || "//:0"}/>
                {text}
            </div>
        );
    }

    getListStyle() {
        return list;
    }

    getCommonStyle() {
        return commons;
    }
}
