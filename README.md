TV Portal

This project implements a portal, aimed to be embedded where ever you want (for instance in a tv !).
It offers an api which can be implemented to develop new custom modules.
Each module will appear as a source of this TV portal.

Module implementation
=====================

A module must be a class which implements the following functions:

- constructor(api)
- *getRoot(): item
- *getData(item): Promise([item,...])
- +getSyncViewOnCreate(item): HTML || JSX
- +getASyncViewOnCreate(item): Promise(HTML || JSX)
- getSyncViewOnFocus(item): HTML || JSX
- getASyncViewOnFocus(item): Promise(HTML || JSX)
- onFocus(item): Promise(item)
- onClick(item): Promise(item)
- *serialize(item): String
- *deserialize(String): item
- *accept(itemOrString): boolean

function with *: mandatory
function with +: only one is mandatory

*getRoot(): item
----------------
This method should return an object which represents your module.

It will be displayed in the application's source list as a new entry.
Its representation in the application's source list will follow the same rendering lifecycle (see rendering lifecycle paragraph) as all other items.

*getData(item): Promise([item,...])
-----------------------------------
This method must return a Promise.

When the promise resolves, it should return an array of object representing the sub-items of the given item. If there is no sub-items for the given item, the promise should return undefined, null or an empty array.

When the promise rejects, it should return an error message.
The sub-items will be graphically represented in a new panel and will follow the rendering lifecycle (see rendering lifecycle paragraph).

+getSyncViewOnCreate(item): HTML || JSX
---------------------------------------
Should return an html or jsx representation of the given item

+getAsyncViewOnCreate(item): Promise(HTML || JSX)
-------------------------------------------------
Should also return a html or jsx representation of the given item but in an asynchronous way (i.e through a promise). This gives you the ability to retrieve some futher information on the given item,
for example through a xhr call, before actually returning the view.

+getSyncViewOnFocus(item): HTML || JSX
---------------------------------------
Should return an html or jsx representation of the given item when it is focused

+getAsyncViewOnFocus(item): Promise(HTML || JSX)
-------------------------------------------------
Should also return a html or jsx representation of the given item when it is focused but in an asynchronous way (i.e through a promise).

onFocus(item): Promise(ContentAction)
-------------------------------------
if present in the module, onFocus method is called each time an item is focused. You should return a ContentAction object if you want to load somehing in the content space of the portal (see ContentAction pragraph to get more details on that)

onClick(item): Promise(ContentAction)
----------------------------
if present in the module, onClick method is called each time an item is focused.You should return a ContentAction object if you want to load somehing in the content space of the portal (see ContentAction pragraph to get more details on that)

ContentAction Object
--------------------
A ContentAction object must contain the followig structure :
- *type: String (TYPE_VIEW|TYPE_URL|TYPE_MEDIA)
- *src: View (see View Object)|String|MediaDataSource(see MediaDataSource Object & Media Object) depending on the type of your action
- isSilent: Boolean

(*Attributes with * are mandatory*)

If is Silent = true, then the content space of the portal will load the medias without affecting the screen state (menu stays open, focus stays where it is,...)

View Object
-----------
A View object is an object which must expose the following attributes:
- *value: String or JSX
- logo: String
- title: String

MediaDataSource Object
----------------------
A MediaDataSource object is either
- Array(media objects)

or

- a DataSource Object : { next(): Promise(media object) }


Media Object
------------
A media object must expose the following attributes:
- *type: String (video, image, audio,... check out PlayerFactory types)
- *url: String
- title: String
- logo: String

(*Attributes with * are mandatory*)

Items' rendering lifecycle
--------------------------
When rendering an item in the menu, the framework first calls :
- **getSyncViewOnCreate(item): HTML || JSX**

If the method is not present, it will render an empty tile.
Then, it will try to call :
- **getASyncViewOnCreate(item): Promise(HTML || JSX)**

If the promise returns a view, the item's view will be updated with that view.
Now, if an element receives the focus, the framework will look into your module in order to find :
- **getSyncViewOnFocus(item): HTML || JSX**

The method will be called if present and your item's view will be updated with the result of that synchronous call.
The framework will then look into your module to find :
- **getAsyncViewOnFocus(item): Promise(HTML|JSX)**

Once again, the method will be called if present, the view returned through the promise will be use to update
your item's view while it stays focused.

When your item gets blurred, it is rerendered with getSyncViewOnCreate and getAsyncViewOnCreate.

How to run the project
======================
1. install nodeJS (https://nodejs.org/)
2. checkout the project
3. npm install
3. npm run build (or npm run watch if you want the build to be regenerated each time you modify a file)
4. npm run start
