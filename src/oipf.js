if (!window.oipfObjectFactory) {
    const config = {
        emitterMaxListeners: 10, // Override this default value if needed
        emulateImageSettingsOnVB: true, // Default
        "default": {
            vendorName: "Code Lutin"
        }
    };

    const oipf = window.__oipf__;

    // Init oipf context
    const context = window.ctx = new oipf.OipfStubContext(config);

    window.ctx.dataProvider = new oipf.JsonTvProvider({
        maxDate: Date.now() + 3600 * 1000 * 4, // 4 hours after now
        endpoint: "//proxy-xmltv.nuiton.org/tnt.json"
        // endpoint: "//192.168.99.124:9080/tnt.json"
    });

    // Creation the stubbed oipfObjectFactory
    window.oipfObjectFactory = context.createOipfObjectFactory();
}
