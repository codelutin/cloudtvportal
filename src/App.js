/*
 * Cloud TV Portal, (C) 2015 Code Lutin (SAS).
 * Copyright (C) <year>  <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import React from "react";
import ReactDom from "react-dom";

import {FwkApi} from "./js/utils/FwkApi";
import {keysMap} from "./js/utils/KeysMap";
import {eventsHandler} from "./js/utils/EventsHandler";

import {Portal} from "./js/structures/Portal";
import {HlsVideo} from "./js/components/HlsVideo";

import {DefaultTheme} from "themes/AvailableThemes";

// Registering tv remote keys handled by the application
keysMap.registerKeysForApplication(keysMap.NAVIGATION | keysMap.INFO, []);

// Register custom elements
document.registerElement("hls-video", {
    prototype: Object.create(HlsVideo.prototype),
    "extends": "video"
});

// Let's instantiate Api object
const api = new FwkApi();
api.theme = new DefaultTheme(api); // Setup app's theme

// Importing modules
import {Tv} from "modules/tv/js/Tv";
import {Tutorial} from "modules/tutorial/js/Tutorial";
import {Webdav} from "modules/webdav/js/Webdav";
import {Ott} from "modules/ott/js/Ott";
api.addModule(Tv);
api.addModule(Ott);
api.addModule(Webdav);
api.addModule(Tutorial);

// Registering listeners
window.addEventListener("keydown", (e) => { eventsHandler.handleKeyDown(e); });
window.addEventListener("load", () => {
    // Appending portal structure into the dom
    const portal = ReactDom.render(
        <Portal api={api} eventsHandler={eventsHandler} />,
        document.getElementById("app")
    );
    eventsHandler.focus(portal);
});
