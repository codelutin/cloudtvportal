/*
 * Cloud TV Portal, (C) 2015 Code Lutin (SAS).
 * Copyright (C) <year>  <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import {singleton} from "./Singleton";

class KeysMap {

    constructor() {
        // Oipf Constants
        this.RED = 0x1;
        this.GREEN = 0x2;
        this.YELLOW = 0x4;
        this.BLUE = 0x8;
        this.NAVIGATION = 0x10;
        this.VCR = 0x20;
        this.SCROLL = 0x40;
        this.INFO = 0x80;
        this.NUMERIC = 0x100;
        this.ALPHA = 0x200;
        this.OTHER = 0x400;

        this.eventsMap = new Map();

        // On PC
        this.eventsMap.set(13, "onOk");
        this.eventsMap.set(27, "onBack");
        this.eventsMap.set(37, "onLeft");
        this.eventsMap.set(38, "onUp");
        this.eventsMap.set(39, "onRight");
        this.eventsMap.set(40, "onDown");
        this.eventsMap.set(73, "onInfo"); // Alt + I

        // On tv
        this.eventsMap.set(461, "onBack"); // Back
        this.eventsMap.set(457, "onInfo"); // Info +
    }

    getEventName(event) {
        return this.eventsMap.get(event.keyCode);
    }

    getEventsNames() {
        return this.eventsMap.values();
    }

    registerKeysForApplication(keys, others) {
        if (window.ApplicationManager) {
            const w = window.ApplicationManager.getWindowByName("UI");
            w && w.keySet.setValue(keys, others);
        }
    }
}

export const keysMap = singleton(KeysMap);
