/*
 * Cloud TV Portal, (C) 2015 Code Lutin (SAS).
 * Copyright (C) <year>  <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import {EventEmitter} from "./EventEmitter";
import {logger} from "./Logger";
import {syncStorage} from "./SyncStorage";
import {asyncStorage} from "./AsyncStorage";
let moment = require("moment");
let XMLUtils = require("exml-easysax");
let XMLParser = XMLUtils.Parser;

export class FwkApi extends EventEmitter {

    get TYPE_URL() { return "url"; }
    get TYPE_MEDIA() { return "media"; }
    get TYPE_VIEW() { return "view"; }

    constructor() {
        super();

        const oc = this._openCallbacks = new Map();
        oc.set(this.TYPE_URL, this.emit.bind(this, "openUrl"));
        oc.set(this.TYPE_MEDIA, this.emit.bind(this, "openMedia"));
        oc.set(this.TYPE_VIEW, this.emit.bind(this, "openView"));

        this.moduleById = new Map();
        this.idByModule = new Map();

        // Instanciate framework utilities
        this.logger = logger;
        this.syncStorage = syncStorage;
        this.asyncStorage = asyncStorage;
        this.XMLParser = XMLParser;
        this.moment = moment;
    }

    addModule(ModuleClazz) {
        if (!ModuleClazz) {
            this.logger.error("[Api] - addModule requires a module as a parameter");
        } else {
            const moduleId = this.nonce(),
                module = new ModuleClazz(this, moduleId);

            this.moduleById.set(moduleId, module);
            this.idByModule.set(module, moduleId);
        }
    }

    findModule(item) {
        let result = this.moduleById.get(item && item.moduleId);

        if (!result) {
            const modules = this.moduleById.values();
            for (let m of modules) {
                if (m.accept(item)) {
                    result = m;
                    break;
                }
            }
        }

        return result;
    }

    getModulesRoots() {
        return Array.from(this.moduleById.values(), m => m.getRoot());
    }

    getModuleId(module) {
        return this.idByModule.get(module);
    }

    /**
     * CAREFUL: THIS API SHOULDN'T BE USED BY ANY MODULE AND IS VERY LIKELY TO DISAPEAR SOON
     *
     * Open some content in the content space of the portal
     *
     * A contentAction object must contain the followig structure :
     * - *type: String (TYPE_VIEW|TYPE_URL|TYPE_MEDIA)
     * - *src: View|String|MediaDataSource depending on the type of your acton
     * - isSilent: Boolean
     *
     * If is Silent = true, then the content space of the portal will load the medias
     * without affecting the screen state (menu stays open, focus stays where it is,...)
     *
     * A View object is an object which must expose the following attributes:
     * - *value: String or JSX
     * - logo: String
     * - title: String
     *
     * A MediaDataSource object is either Array(media objects) or a DataSource Object { next(): Promise(media object)}
     * A media object must expose the following attributes:
     * - *type: String
     * - *url: String
     * - title: String
     * - logo: String
     */
    _open(contentAction) {
        const method = this._openCallbacks.get(contentAction.type);
        method && method(contentAction.src, contentAction.isSilent);
    }

    makePromiseCancelable(promise) {
        let isCanceled = false;
        const continueIfStillAlive = (cb, value) => {
            if (!isCanceled) {
                cb(value);
            }
        };

        const wrapper = new Promise(function(resolve, reject) {
            promise.then(
                (v) => continueIfStillAlive(resolve, v),
                (v) => continueIfStillAlive(reject, v)
            );
        });

        wrapper.cancel = () => {
            isCanceled = true;
        };

        return wrapper;
    }

    ensurePromise(promise, methodName) {
        let result = promise;
        if (!promise) {
            result = Promise.reject();
        } else if (promise && !promise.then) {
            this.logger.warn(methodName || "Warning", " : expected Promise, received ", promise);
            result = Promise.reject();
        }
        return result;
    }

    nonce() {
        this._nonce = this._nonce || Date.now();
        return this._nonce++;
    }
}
