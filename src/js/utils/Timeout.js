/*
 * Cloud TV Portal, (C) 2015 Code Lutin (SAS).
 * Copyright (C) <year>  <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 /**
  * This is a simple timer utility wrapped as promise.
  *
  * Example :
  * ---------
  * $timeout(1000)
  * .then(() => console.log("fired !"));
  *
  * Api :
  * -----
  * constructor(delay, isIntervall) :
  *     - delay : time in ms before the timer is triggered (ie. the promise is resolved)
  *     - isIntervall: if true, the timer behaves as an intervall. The timer is re armed after it fired
  * cancel() : stop the timer and reject the promise
  */
 export class Timeout {

     constructor(delay, isIntervall) {
         this.delay = delay;
         this.isIntervall = !!isIntervall;

         this._callbacks = [];

         this.fire();
     }

     fire() {
         this._currentPromise = new Promise((resolve, reject) => {
             this._currentPromiseCbs = {resolve, reject};
             this._timerId = setTimeout(() => {
                 resolve();
                 this.isIntervall && this.fire();
             }, this.delay);
         });

         // Refire the callbacks when the current promise is resolved / rejected
         for (let i = 0, l = this._callbacks.length; i < l; i++) {
             this._currentPromise.then(...this._callbacks[i]);
         }
     }

     then(...cbs) {
         this._callbacks.push(cbs);
         return this._currentPromise.then(...cbs);
     }

     catch(cb) {
         this._callbacks.push([() => {}, cb]);
         return this._currentPromise.catch(cb);
     }

     cancel() {
         clearTimeout(this._timerId);
         this._currentPromiseCbs.reject();
     }
 }

 export const timeout = (...args) => {
     return new Timeout(...args);
 };
