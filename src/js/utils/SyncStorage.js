/*
 * Cloud TV Portal, (C) 2015 Code Lutin (SAS).
 * Copyright (C) <year>  <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 import {singleton} from "./Singleton";

 class SyncStorage {

     set(key, value) {
         let jKey = JSON.stringify(key);
         let jValue = JSON.stringify(value);
         localStorage.setItem(jKey, jValue);
     }

     get(key) {
         let jKey = JSON.stringify(key);
         let jValue = localStorage.getItem(jKey);
         let value = jValue && JSON.parse(jValue);
         return value;
     }

     setRaw(key, value) {
         localStorage.setItem(key, value);
     }

     getRaw(key) {
         return localStorage.getItem(key);
     }

 }

 export const syncStorage = singleton(SyncStorage);
