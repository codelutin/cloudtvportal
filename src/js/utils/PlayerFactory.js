/* eslint no-console: 0 */
/*
 * Cloud TV Portal, (C) 2015 Code Lutin (SAS).
 * Copyright (C) <year>  <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import {singleton} from "./Singleton";
import {VideoPlayer} from "../components/VideoPlayer";

class PlayerFactory {

    static get AUDIO_TYPE() { return "audio"; }
    static get IMAGE_TYPE() { return "image"; }
    static get VIDEO_TYPE() { return "video"; }

    get(type) {
        switch (type) {

        case PlayerFactory.VIDEO_TYPE:
            return VideoPlayer;
        case PlayerFactory.AUDIO_TYPE:
        case PlayerFactory.IMAGE_TYPE:
        default:
            return "";

        }
    }
}

export const playerFactory = singleton(PlayerFactory);
