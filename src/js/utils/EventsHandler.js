/*
 * Cloud TV Portal, (C) 2015 Code Lutin (SAS).
 * Copyright (C) <year>  <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import react from "react";
import reactDOM from "react-dom";

import {keysMap} from "./KeysMap";
import {logger} from "./Logger";
import {singleton} from "./Singleton";

export class EventsHandler {

    get STORE_PREFIX() { return "STORE"; }

    constructor() {
        this.stores = {};
        this._storeUniqueIndex = 0;
    }

    /**
     * Handling keydown events
     *
     * This is a hack, should be refactor and use proper synthetic events...
     */
    handleKeyDown(e) {
        let name = keysMap.getEventName(e);

        this.bubble(this.activeElement, name, e);

        e.preventDefault();
        e.stopPropagation();
    }

    /***************************************************************
     * Next methods address DOM messaging
     * allowing the user to communicate between components through
     * DOM events
     ***************************************************************/

    /**
     * This function bubbles an event towards the dom tree from the given node to the top of the hierarchy
     * unless one the node returns false when receinving the event.
     */
    bubble(node, eventName, ...params) {
        if (node) {
            const continuePropagation = !node[eventName] || node[eventName](...params) !== false;

            if (continuePropagation && node !== document.body) {
                this.bubble(node.parentNode, eventName, ...params);
            }
        }
    }

    /**
     * Sets a virtual focus on the given node, triggering onFocus and onBlur event on this node and on its top hierarchy
     */
    focus(nodeOrComponent, ...params) {
        const node = reactDOM.findDOMNode(nodeOrComponent);
        const blurredElement = this.activeElement;
        const event = {target: node, blurTarget: blurredElement, params};

        this._focusChainLevel = this._focusChainLevel || 0;

        if (!node) {
            logger.warn("[Warning] - EventsHandler : you tried to put the focus on a node which doesn't exist.");
        } else {

            // If we're not in the middle of a focus chain,
            // let's blurred the currently active element
            if (this._focusChainLevel === 0 && blurredElement) {
                blurredElement.classList.remove("focused");
                this.bubble(blurredElement, "onBlur", event);
            }

            this._focusChainLevel++;

            // Set the given node as the new activeElement and propagate the focus event
            this.activeElement = node;
            node.didReceiveFocus && node.didReceiveFocus(event);
            this.bubble(this.activeElement, "onFocus", event);

            this._focusChainLevel--;

            this.activeElement.classList.add("focused");
        }
    }

    bindEvents(instance, node) {
        // Binding key events
        const events = keysMap.getEventsNames();
        for (let e of events) {
            this.bindEvent(e, instance, node);
        }

        // Binding focus events
        this.bindEvent("onBlur", instance, node);
        this.bindEvent("onFocus", instance, node);
        this.bindEvent("didReceiveFocus", instance, node);
    }

    bindEvent(e, instance, node) {
        if (node && instance[e]) {
            node[e] = instance[e].bind(instance);
        }
    }

    /***************************************************************
     * Next methods address action messaging
     * allowing the components to communication through actions
     * and stores which act like model object to persist some state
     *
     * This is a simple flux implementation.
     ***************************************************************/

    registerStore(callback) {
        const tokenId = this.STORE_PREFIX + this._storeUniqueIndex++;
        this.stores[tokenId] = callback;
        return tokenId;
    }

    unregisterStore(id) {
        delete this.stores[id];
    }

    dispatchAction(action) {
        const ids = Object.keys(this.stores);
        for (let i = 0, l = ids.length; i < l; i++) {
            this.stores[ids[i]](action);
        }
    }
}

export const eventsHandler = singleton(EventsHandler);
