/*
 * Cloud TV Portal, (C) 2015 Code Lutin (SAS).
 * Copyright (C) <year>  <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This class is a simple event emitter.
 *
 * Api:
 * ----
 * - on(event, callback) : register a callback on a specific event
 * - once(event, callback) : register a callback on a specific event but it will be triggered only once and removed from the callback after that first shot
 * - emit(event, ...params) : trigger an event with some optional parameters which will be forwarded to the registered callbacks
 * - clear(event, callback) : remove a callback set on the given event.
 *                            If no callback, it will remove all callbacks set on that event.
 *                            If no paraemters, it will remove all listeners from all events
 */
export class EventEmitter {

    constructor() {
        this.callbacks = {};
    }

    on(event, callback) {
        const callbacks = this.callbacks[event] || [];
        callbacks.push(callback);
        this.callbacks[event] = callbacks;
    }

    once(event, callback) {
        const _cb = (...params) => {
            callback(...params);
            this.clear(event, callback);
        };

        this.on(event, _cb);
    }

    emit(event, ...params) {
        const callbacks = this.callbacks[event] || [];
        for (let i = 0, l = callbacks.length; i < l; i++) {
            callbacks[i](...params);
        }
    }

    clear(event, callback) {
        if (!event) {
            this.callbacks = {};
        } else if (!callback) {
            this.callbacks[event] = [];
        } else {
            const callbacks = this.callbacks[event];
            callbacks.splice(callbacks.indexOf(callback), 1);
        }
    }
}
