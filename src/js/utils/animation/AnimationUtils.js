/*
 * Cloud TV Portal, (C) 2015 Code Lutin (SAS).
 * Copyright (C) <year>  <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
export class AnimationUtils {
    static multiply(matrix1, matrix2) {
        const m1 = matrix1 || AnimationUtils.newI3(),
            m2 = matrix2 || AnimationUtils.newI3();

        const s = m1[0] * m2[0],
            tx = m1[0] * m2[1] + m1[1],
            ty = m1[0] * m2[2] + m1[2];

        return [s, tx, ty];
    }

    static newI3() {
        return [1, 0, 0];
    }
}
