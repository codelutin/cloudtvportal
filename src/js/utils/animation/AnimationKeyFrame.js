/*
 * Cloud TV Portal, (C) 2015 Code Lutin (SAS).
 * Copyright (C) <year>  <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import {AnimationUtils} from "./AnimationUtils.js";

export class AnimationKeyFrame {

    constructor() {
        this.descriptor = {};
    }

    translateX(tx) {
        return this.translateXY(tx, 0);
    }

    translateY(ty) {
        return this.translateXY(0, ty);
    }

    translateXY(tx, ty) {
        return this._add("transform", [1, tx, ty]);
    }

    opacity(value) {
        return this._add("opacity", value);
    }

    toKeyFrame(resultObject) {
        const result = resultObject || {};
        const properties = Object.keys(this.descriptor);

        let prop, value;
        for (let i = 0, l = properties.length; i < l; i++) {
            prop = properties[i];
            value = this.descriptor[prop];

            if (prop === "transform") {
                result[prop] = "matrix3d(" + value[0] + ", 0, 0, 0, 0, " + value[0] + ", 0, 0, 0, 0, 1, 0," + value[1] + "," + value[2] + ", 0, 1)";
            } else {
                result[prop] = value;
            }

        }

        return result;
    }

    _add(property, value) {
        switch (property) {
        case "transform":
            this.descriptor[property] = AnimationUtils.multiply(this.descriptor[property], value);
            break;
        default:
            this.descriptor[property] = (this.descriptor[property] || 0) + (value || 0);
            break;
        }
        return this;
    }

    apply(...nodes) {
        let node;
        for (let i = 0, l = nodes.length; i < l; i++) {
            node = nodes[i];
            this.toKeyFrame(node.style);
            node._keyframe = this;
        }
    }
}
