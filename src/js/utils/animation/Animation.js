/*
 * Cloud TV Portal, (C) 2015 Code Lutin (SAS).
 * Copyright (C) <year>  <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This is a basic abstraction around element.animate
 * Checkout https://developer.mozilla.org/en-US/docs/Web/API/Element/animate to get more info on that
 *
 * The constructor takes keyframes and animation options as parameters.
 *
 * Example :
 * const firstKeyFrame = new AnimationKeyFrame().translateX(100),
 *       secondKeyFrame = new AnimationKeyFrame().translateX(200);
 *
 * const myAnim = new Animation([firstKeyFrame, secondKeyFrame], {duration: 500});
 *
 * myAnim.apply(document.body).then(() => { console.log("anim just finished"); });
 *
 * Note :
 * This abstraction should probably replaced or adjusted with new KeyFrameEffect api (https://developer.mozilla.org/en-US/docs/Web/API/KeyframeEffect)
 */
export class Animation {

    static get DEFAULT_ANIM_DURATION() { return 200; }

    constructor(keyframes, options) {
        this.frames = keyframes.map((k) => k.toKeyFrame());

        let property;
        const optionsNames = Object.keys(options);
        for (let i = 0, l = optionsNames.length; i < l; i++) {
            property = optionsNames[i];
            this[property] = options[property];
        }
    }

    get duration() {
        return this._duration || Animation.DEFAULT_ANIM_DURATION;
    }

    set duration(duration) {
        this._duration = duration;
    }

    get timingFunction() {
        return this._timing || "ease-in-out";
    }

    set timingFunction(tf) {
        this._timing = tf;
    }

    get iterations() {
        return this._iterations || 1;
    }

    set iterations(count) {
        this._iterations = count;
    }

    get delay() {
        return this._delay || 0;
    }

    set delay(delay) {
        this._delay = delay;
    }

    get fill() {
        return this._fill;
    }

    set fill(fill) {
        this._fill = fill;
    }

    apply(...nodes) {
        const result = [];

        let node;
        for (let i = 0, l = nodes.length; i < l; i++) {
            node = nodes[i];

            result.push(new Promise(this._execute.bind(this, node)));
        }

        return result;
    }

    _execute(node, resolve) {
        node._player = node.animate(this.frames, this._getParams());
        node._player.onfinish = () => {
            node._player = null;
            resolve();
        };
    }

    _getParams() {
        return {
            fill: this.fill,
            iterations: this.iterations,
            duration: this.duration,
            delay: this.delay,
            easing: this.timingFunction
        };
    }
}
