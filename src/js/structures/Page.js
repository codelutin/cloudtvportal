/*
 * Cloud TV Portal, (C) 2015 Code Lutin (SAS).
 * Copyright (C) <year>  <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import React from "react";
import {Component} from "../components/Component";

export class Page extends Component {

    onBack() {
        // this.context.eventsHandler.bubble(this.nodeRoot, "close");
    }

    render() {
        return this.props.children;
    }
}

Page.childContextTypes = {
    api: React.PropTypes.object,
    eventsHandler: React.PropTypes.object
};

Page.propTypes = {
    api: React.PropTypes.object,
    children: React.PropTypes.element,
    eventsHandler: React.PropTypes.object
};
