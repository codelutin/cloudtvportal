/*
 * Cloud TV Portal, (C) 2015 Code Lutin (SAS).
 * Copyright (C) <year>  <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import React from "react";

import {Page} from "../structures/Page";

import {Header} from "../components/Header";
import {MediaBrowser} from "../components/MediaBrowser";
import {Transitioner} from "../components/Transitioner";
import {Viewer} from "../components/Viewer";
import {ContentActionCreator} from "../actions/ContentActionCreator";

export class Portal extends Page {

    constructor() {
        super();
        this._menuVisible = true;

        this.state = {};
    }

    getChildContext() {
        return {
            api: this.props.api,
            eventsHandler: this.props.eventsHandler
        };
    }

    willMount() {
        const {api} = this.props;
        api.on("openUrl", (...args) => this.openContentUrl(...args));
        api.on("openView", (...args) => this.openContentView(...args));
        api.on("openMedia", (...args) => this.openContentMedia(...args));
    }

    openContentUrl(url, isSilent) {
        this._setSilentMode(isSilent);

        if (!isSilent) {
            this.hideMenu()
            .then(() =>
                ContentActionCreator.updateContent("img/network.png", null, url, url) // Update header
            );
        }

        this.refs.router.open(
            <Page>
                <iframe className="fit" id="iframe" key={url} src={url} />
            </Page>
        );
    }

    openContentView(view, isSilent) {
        this._setSilentMode(isSilent);

        if (!isSilent) {
            this.hideMenu()
            .then(() => {
                // Only update header if there is something to display
                if (view.logo || view.title) {
                    ContentActionCreator.updateContent(view.logo, null, view.title);
                }
            });

        }

        this.refs.router.open(view.value);
    }

    openContentMedia(dataSource, isSilent) {
        this._setSilentMode(isSilent);

        if (!isSilent) {
            this.hideMenu();
        }

        // FIXME : datasource should have a unique key
        this.refs.router.open(
            <Page>
                <MediaBrowser dataSource={dataSource} key={JSON.stringify(dataSource)} />
            </Page>
        );
    }

    _setSilentMode(isSilent) {
        this.state.silent = isSilent;
        this.setState(this.state);
    }

    onInfo() {
        if (!this._menuVisible) {
            this.refs.header.toggle();
        }
    }

    onBack() {
        this.toggleMenuVisibility();
    }

    toggleMenuVisibility() {
        if (this._menuVisible) {
            this.hideMenu();
        } else {
            this.showMenu();
        }
    }

    hideMenu() {
        this._menuVisible = false;
        this.refs.menuContainer.classList.add("hide");
        return this.refs.menu.close().then(() => {
            // When ending the close details anim, let's check if the menu visibility flag is still false
            if (!this._menuVisible) {
                // If true, let's handle the focus
                this.props.eventsHandler.focus(this.refs.router);

            } else {
                // if false, someone asked to show while the details panel were hiding, let's just show the menu
                this.showMenu();
            }
        });
    }

    showMenu() {
        this.refs.header.hide();
        this._menuVisible = true;
        this.refs.menuContainer.classList.remove("hide");
        this.props.eventsHandler.focus(this.refs.menu);
    }

    didReceiveFocus() {
        if (this._menuVisible) {
            this.props.eventsHandler.focus(this.refs.menu);
        }
    }

    render() {
        return (
            <div className="portal">
                <div className="portal-content gl">
                    <Transitioner ref="router" silent={this.state.silent} />
                </div>
                <div className="portal-header gl">
                    <Header ref="header" silent={this.state.silent} />
                </div>
                <div className="portal-menu gl" ref="menuContainer">
                    <Viewer dataSource={this.props.api.getModulesRoots()} ref="menu"/>
                </div>
            </div>
        );
    }

}

Portal.propTypes = {
    api: React.PropTypes.object,
    eventsHandler: React.PropTypes.object
};

Portal.childContextTypes = {
    api: React.PropTypes.object,
    eventsHandler: React.PropTypes.object
};
