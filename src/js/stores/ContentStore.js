/*
 * Cloud TV Portal, (C) 2015 Code Lutin (SAS).
 * Copyright (C) <year>  <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import {Store} from "./Store";
import {singleton} from "../utils/Singleton";

import {ContentActionCreator} from "../actions/ContentActionCreator";

class ContentStore extends Store {

    get CHANGE_EVENT() { return "update"; }

    handlePayload(action) {
        switch (action.type) {
        case ContentActionCreator.CONTENT_UPDATE:
            this.updateStore(action);
            this.emitChange();
            break;
        default:
            break;
        }
    }

    updateStore(action) {
        this.name = action.name;
        this.description = action.description;
        this.logo = action.logo;
        this.src = action.src;
    }

    emitChange() {
        this.emit(this.CHANGE_EVENT, this);
    }
}

export const contentStore = singleton(ContentStore);
