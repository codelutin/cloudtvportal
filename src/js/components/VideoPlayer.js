/*
 * Cloud TV Portal, (C) 2015 Code Lutin (SAS).
 * Copyright (C) <year>  <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import React from "react";

import {Player} from "./Player";

export class VideoPlayer extends Player {

    static get STATUS_ERROR() { return -1; }
    static get STATUS_STOPPED() { return 0; }
    static get STATUS_PLAYING() { return 1; }
    static get STATUS_PAUSED() { return 2; }

    constructor() {
        super();
        this._supportedProps = new Set();
        this._supportedProps.add("autoPlay");
        this._supportedProps.add("loop");
        this._supportedProps.add("mute");
        this._supportedProps.add("volume");
    }

    get playerNode() {
        return this.refs.playerNode;
    }

    didMount() {
        this._registerHandlers();

        // Handling src attriute manually so that we can map it to the source attribute of hls video elements
        const src = this.props.src;
        if (src) {
            this.load(src);
        }

    }

    willUnmount() {
        this.release();
    }

    load(url) {
        this.currentUrl = url || this.currentUrl;
        this.playerNode.source = this.currentUrl;
        this.playerNode.load();
    }

    play() {
        this.playerNode.play();
    }

    pause() {
        this.playerNode.pause();
    }

    stop() {
        this.playerNode.release();
    }

    release() {
        this.playerNode.source = "";
    }

    _registerHandlers() {
        this.playerNode.addEventListener("playing", (e) => {
            this.playerStatus = VideoPlayer.STATUS_PLAYING;
            this.emit(Player.PLAY_EVENT, e);
        });
        this.playerNode.addEventListener("pause", (e) => {
            this.playerStatus = VideoPlayer.STATUS_PAUSED;
            this.emit(Player.PAUSE_EVENT, e);
        });
        this.playerNode.addEventListener("ended", (e) => {
            this.playerStatus = VideoPlayer.STATUS_STOPPED;
            this.emit(Player.END_EVENT, e);
        });
        this.playerNode.addEventListener("error", (e) => {
            this.playerStatus = VideoPlayer.STATUS_ERROR;
            this.emit(Player.ERROR_EVENT, e);
        });
        this.playerNode.addEventListener("abort", (e) => {
            this.playerStatus = VideoPlayer.STATUS_STOPPED;
            this.emit(Player.ABORT_EVENT, e);
        });
    }

    _applySupportedProps() {
        const result = Object.assign({}, this.props);
        const propsNames = Object.keys(result);

        let p;
        for (let i = 0, l = propsNames.length; i < l; i++) {
            p = propsNames[i];
            if (!this._supportedProps.has(p)) {
                delete result[p];
            }
        }

        return result;
    }

    render() {
        return <video class="player player--video" is="hls-video" ref="playerNode" {...this._applySupportedProps()} />;
    }
}
