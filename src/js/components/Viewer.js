/*
 * Cloud TV Portal, (C) 2015 Code Lutin (SAS).
 * Copyright (C) <year>  <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import React from "react";
import ReactDOM from "react-dom";

import {List} from "./List";
import {Component} from "./Component";

import {Animation} from "../utils/animation/Animation";
import {AnimationKeyFrame} from "../utils/animation/AnimationKeyFrame";

export class Viewer extends Component {

    constructor() {
        super();
        this.state = {details: null};

        this._openedKeyFrame = new AnimationKeyFrame().translateX(0);
        this._animOptions = {fill: "forwards"};
    }

    willMount() {
        this.detailsKey = this.context.api.nonce();
    }

    onOk() {
        const {api} = this.context;
        const list = this.refs.items,
            crtItem = list && list.getCurrentItemData();

        const module = api.findModule(crtItem);
        if (module && module.onClick) {
            // Stop current promise before triggering a new one
            this._clickPromise && this.clickPromise.cancel();

            this._clickPromise = api.ensurePromise(module.onClick(crtItem), "[Module] - onClick");
            this._clickPromise = api.makePromiseCancelable(this._clickPromise);
            this._clickPromise.then((action) => this.context.api._open(action));
        }

        return false;
    }

    onUp() {
        const list = this.refs.items,
            crtIndex = list.currentIndex;

        list.prev().then(
            () => this._refreshDetails(crtIndex),
            () => this._refreshDetails(crtIndex)
        );

        return false;
    }

    onDown() {
        const list = this.refs.items,
            crtIndex = list.currentIndex;

        list.next().then(
            () => this._refreshDetails(crtIndex),
            () => this._refreshDetails(crtIndex)
        );

        return false;
    }

    _refreshDetails(index) {
        if (this.refs.items.currentIndex !== index) {
            // Open details corresponding to the newly selected item
            this.selectCurrentItemDetails();
        }
    }

    onLeft() {

        // Close the details of the currently selected item
        const {parent} = this.props;
        if (parent) {
            this.context.eventsHandler.focus(parent);
            this.closeDetails();
        }

        return false;
    }

    onRight() {
        this.context.eventsHandler.focus(this.refs.details);
        return false;
    }

    didReceiveFocus() {
        // Focus on list component
        this.context.eventsHandler.focus(this._getListNode());

        // Open first level details
        if (!this.state.details) {
            const list = this.refs.items;
            this.selectDetails(list && list.getCurrentItemData());
        }
    }

    isActive() {
        const {eventsHandler} = this.context;
        const listNode = this._getListNode();

        return listNode.contains(eventsHandler.activeElement);
    }

    selectCurrentItemDetails() {
        const list = this.refs.items;
        this.selectDetails(list && list.getCurrentItemData());
    }

    selectDetails(item) {
        return this._getDetailsData(item)
        .then(
            (items) => this.openDetails(items),
            (mess) => mess && this.context.api.logger.warn(mess)
        );
    }

    close() {
        let result = Promise.resolve();

        this.prevState = Object.assign({}, this.state);

        const details = this.refs.details;
        if (details && details.close) {
            result = details.close();
        }

        return result.then(() => this.closeDetails());
    }

    openDetails(items) {
        let result = Promise.resolve();
        if (items) {
            this.detailsKey = this.context.api.nonce();
            // Let's display the sub items of the selected items
            this.setState({
                details: items
            });

            const openAnim = this._getOpenAnim(items);

            // Let's open the details view and focus it afterwards
            result = Promise.all(openAnim.apply(this.refs.detailsContainer));
        }

        return result;
    }

    closeDetails() {
        let result = Promise.resolve();

        if (this._closedKeyFrame) {
            const closeAnim = this._getCloseAnim();

            // Remove the details and refocus the current viewer
            result = Promise.all(closeAnim.apply(this.refs.detailsContainer));
            result.then(() => {
                this.setState({details: null});
            });
        }

        return result;
    }

    _computeClosedKeyFrame(items) {
        // Try to compute the sub list width, if it can't be found, we use the default list width
        let width = this.refs.items.defaultStyle.width;

        if (items && items.length) {
            const style = items[0].style;

            // if it's an array of item, let's try to find the item's width
            width = style && style.width || width;
        }

        return new AnimationKeyFrame().translateX(-width);
    }

    _getOpenAnim(items) {
        this._closedKeyFrame = this._computeClosedKeyFrame(items);
        return new Animation([this._closedKeyFrame, this._openedKeyFrame], this._animOptions);
    }

    _getCloseAnim() {
        return new Animation([this._openedKeyFrame, this._closedKeyFrame], this._animOptions);
    }

    _getDetailsData(currentItem) {
        return new Promise((resolve, reject) => {
            if (currentItem) {
                const {api} = this.context,
                    module = api.findModule(currentItem),
                    promise = module.getData(currentItem);

                if (promise && promise.then) { // ensure it's a promise
                    promise.then(
                        resolve,
                        (err) => reject("[Viewer] - Couldn't retrieve sub items of item " + currentItem.id + ",", err)
                    );
                } else {
                    reject("[Module] - getData(item) method must return a promise, couldn't retrieve sub items of item " + currentItem.id);
                }
            } else {
                reject();
            }
        });
    }

    _getListNode() {
        return ReactDOM.findDOMNode(this.refs.items);
    }

    _renderDetails(items, refName) {
        let result = "";

        if (items && items.length) {
            // Let's check if a size has been defined for the list viewport
            const crtItem = this.refs.items.getCurrentItemData();
            const listSize = crtItem && crtItem.style && crtItem.style.visibleItemsSize;

            result = <Viewer dataSource={items} key={this.detailsKey} listSize={listSize} parent={this} ref={refName} />;
        }

        return result;
    }

    render() {
        return (
            <div className="viewer">
                <div className="viewer-list">
                    <List dataSource={this.props.dataSource} key={this.props.dataSource} ref="items" size={this.props.listSize || 10} />
                </div>
                <div className="viewer-details gl" ref="detailsContainer">
                    {this._renderDetails(this.state.details, "details")}
                </div>
            </div>
        );
    }
}

Viewer.propTypes = {
    dataSource: React.PropTypes.array,
    listSize: React.PropTypes.number,
    parent: React.PropTypes.object
};
