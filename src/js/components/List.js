/*
 * Cloud TV Portal, (C) 2015 Code Lutin (SAS).
 * Copyright (C) <year>  <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import React from "react";

import {Animation} from "../utils/animation/Animation";
import {AnimationKeyFrame} from "../utils/animation/AnimationKeyFrame";

import {Component} from "./Component";

/**
 * This is a basic implementation of a list component.
 *
 * List's props :
 * --------------
 * - dataSource: array of object to diplay
 * - size: number <of visible items in the viewport
 */
export class List extends Component {

    static get DEFAULT_SIZE() { return 10; }
    static get EXTRA_TILE_COUNT() { return 1; }

    willMount() {
        this.state = {
            startIndex: 0,
            currentIndex: 0,
            size: Math.min(this.props.size || List.DEFAULT_SIZE, this.dataSource.length),
            items: null
        };

        this.state.items = this._createItems(this.dataSource, this.absoluteStartIndex, this.absoluteEndIndex);

        // List items view can change depending on whether they are focused or not,
        // the _frozenItems array is used to save the representation of an item at a given moment.
        // It helps us restore a the view of a given item a a given moment
        // (for instance, the view of an item can change when the item is focused but should be restored on blur)
        this._frozenItems = [];

        this._defaultItemHeight = this._computeFullItemSize(this.defaultStyle.defaultItemHeight);

        // Lets instanciate default animations params
        this._defaultKeyFrames = {
            none: new AnimationKeyFrame().translateY(0),
            prev: new AnimationKeyFrame().translateY(this._defaultItemHeight),
            next: new AnimationKeyFrame().translateY(-this._defaultItemHeight)
        };
        this._animOptions = {timingFunction: "linear"};
        this._defaultAnimations = {
            prev: new Animation([this._defaultKeyFrames.none, this._defaultKeyFrames.prev], this._animOptions),
            next: new Animation([this._defaultKeyFrames.none, this._defaultKeyFrames.next], this._animOptions)
        };
    }

    didReceiveFocus() {
        this._focusItem(this.state.currentIndex);
    }

    get defaultStyle() {
        return this.context.api.theme.getListStyle();
    }

    get currentIndex() {
        return this.state.currentIndex;
    }

    get dataSource() {
        return this.props.dataSource || [];
    }

    get absoluteStartIndex() {
        return this.state.startIndex - List.EXTRA_TILE_COUNT;
    }

    get absoluteEndIndex() {
        return this.state.startIndex + this.state.size + List.EXTRA_TILE_COUNT;
    }

    getCurrentItemData() {
        return this.dataSource[this.state.currentIndex];
    }

    getCurrentItemNode() {
        return this._getItemNode(this.state.currentIndex);
    }

    onBlur(e) {
        // If the list lost the focus, let's restore items state
        if (this.nodeRoot && !this.nodeRoot.contains(e.target)) {
            this._restoreDefaultState();
        }
    }

    next() {
        let result = Promise.resolve(this.state.currentIndex);

        if (this.state.currentIndex < this.state.startIndex + this.state.size - 1) {
            // Let's focus next item
            this.state.currentIndex++;
            this._focusItem(this.state.currentIndex);
            result = Promise.resolve(this.state.currentIndex);

        } else if (this.state.currentIndex < this.dataSource.length - 1) {

            // Let's slide the list container
            result = this._doSlide(1);
        }

        return result;
    }

    prev() {
        let result = Promise.resolve(this.state.currentIndex);

        if (this.state.currentIndex > this.state.startIndex) {
            // Let's focus previous item
            this.state.currentIndex--;
            this._focusItem(this.state.currentIndex);
            result = Promise.resolve(this.state.currentIndex);

        } else if (this.state.currentIndex > 0) {

            // Let's slide the list container
            result = this._doSlide(-1);
        }

        return result;
    }

    _doSlide(step) {
        let result = this._currentAnim;

        if (!this._currentAnim) {
            const anim = step > 0 ? this._getNextAnim(this.state.currentIndex) : this._getPrevAnim(this.state.currentIndex);

            // Let's update the state
            this.state.startIndex += step;
            this.state.currentIndex += step;

            result = this._currentAnim = Promise.all(anim.apply(this.refs.container))
            .then(() => {
                this._currentAnim = null;

                // Re render the list
                this._setItems(this._createItems(this.dataSource, this.absoluteStartIndex, this.absoluteEndIndex));

                this._focusItem(this.state.currentIndex);
            });
        }

        return result;
    }

    _getPrevAnim(index) {
        const itemHeight = this._getItemHeight(index - 1);
        const prevKeyFrame = itemHeight && new AnimationKeyFrame().translateY(itemHeight);
        return prevKeyFrame ? new Animation([this._defaultKeyFrames.none, prevKeyFrame], this._animOptions) : this._defaultAnimations.prev;
    }

    _getNextAnim(index) {
        const itemHeight = this._getItemHeight(index + 1);
        const nextKeyFrame = itemHeight && new AnimationKeyFrame().translateY(-itemHeight);
        return nextKeyFrame ? new Animation([this._defaultKeyFrames.none, nextKeyFrame], this._animOptions) : this._defaultAnimations.next;
    }

    _getItemStyle(index) {
        const itemData = this.dataSource[index];
        return itemData && itemData.style;
    }

    _getItemHeight(index) {
        const itemStyle = this._getItemStyle(index);
        return itemStyle && this._computeFullItemSize(itemStyle.height);
    }

    _computeFullItemSize(innerSize) {
        return innerSize + (2 * this.defaultStyle.itemPadding || 0) + (this.defaultStyle.borderSize * this.defaultStyle.borderCount || 0);
    }

    _setItems(items) {
        // Let's clone the preivous state object to be sure we keep all attributes value
        const state = Object.assign({}, this.state);

        // Update items' list
        state.items = items;

        // Ask react to rerender what needs to be rerendered
        this.setState(state);
    }

    _focusItem(index) {
        // Before focusing a new item, let's restore the default view (blurred view)
        // of the previously focused item (if there was one)
        this._restoreDefaultState();

        // Focus item node
        const {api, eventsHandler} = this.context;

        // Trigger module's focus callback
        const item = this.dataSource[index],
            module = api.findModule(item);

        // Let's connect the module's apis (See README for further documentation)
        // - onFocus
        // - getSyncViewOnFocus
        // - getAsyncViewOnFocus
        if (module) {
            if (module.onFocus) {
                // Stop current promise before triggering a new one
                this._focusPromise && this._focusPromise.cancel();

                this._focusPromise = api.ensurePromise(module.onFocus(item), "[Module] - onFocus");
                this._focusPromise = api.makePromiseCancelable(this._focusPromise);
                this._focusPromise.then(
                    (action) => this.context.api._open(action),
                    () => {}
                );
            }

            if (module.getSyncViewOnFocus) {
                const view = module.getSyncViewOnFocus(item);
                view && this._updateItemView(item, true, view);
            }

            if (module.getAsyncViewOnFocus) {
                this.context.api.ensurePromise(
                    module.getAsyncViewOnFocus(item),
                    "[Module] - getAsyncViewOnFocus"

                ).then((view) => {

                    // We only update the item view if the list still holds the focus
                    // and the given item is still the focused one
                    if (view
                        && this.nodeRoot && this.nodeRoot.contains(this.context.eventsHandler.activeElement)
                        && item === this.getCurrentItemData()) {
                        this._updateItemView(item, true, view);
                    }

                }, () => {});
            }
        }

        eventsHandler && eventsHandler.focus(this._getItemNode(index));
    }

    _getItemNode(index) {
        return this.refs["item" + index];
    }

    _createItems(dataItems, startIndex, endIndex) {
        const result = [];
        const {api} = this.context;
        const asyncUpdate = (itemIndex, item, view) => {
            // if the item is not currently forzen then we update the view
            if (!this._frozenItems.includes(itemIndex)) {
                this._updateItemView(item, false, view);
            }
        };

        let module, item, view;
        for (let i = startIndex, l = endIndex; i < l; i++) {
            item = dataItems[i];
            if (item) {
                module = api.findModule(item);

                view = this._createEmptyView();

                // Lets plug the framework apis of the item's rendering lifecycle
                if (module) {
                    if (module.getSyncViewOnCreate) {
                        // Let's try to render the item synchronously
                        view = module.getSyncViewOnCreate(item);
                    }

                    // If an async view is available on create let's request it
                    // and re render the item when we get the data
                    if (module.getAsyncViewOnCreate) {
                        this.context.api.ensurePromise(
                            module.getAsyncViewOnCreate(item),
                            "[Module] - getAsyncViewOnCreate"
                        )
                        .then(asyncUpdate.bind(this, i, item), () => {});
                    }
                } else {
                    this.context.api.logger.warn("[Module] - No module found for item", item);
                }

                // Push the sync view as a beginning
                result.push(view);

            } else {
                result.push(this._createEmptyView());
            }
        }

        return result;
    }

    _updateItemView(item, addToFrozenItems, view) {
        const absoluteIndex = this.dataSource.indexOf(item);

        // Let's first check if the items is still displayed
        if (this.nodeRoot // If the component is not attached anymore let's stop here
            && this.absoluteStartIndex <= absoluteIndex && this.absoluteEndIndex > absoluteIndex) {

            if (addToFrozenItems) {
                // Saving current item' view to be able to restore it later on
                this._frozenItems = this._frozenItems || [];
                this._frozenItems.push(absoluteIndex);
            }

            // Ok, let's update the view
            this.state.items[absoluteIndex - this.absoluteStartIndex] = view;
            this.setState(this.state);
        }
    }

    _restoreDefaultState() {
        // Restore the default item's view by recreating them
        if (this._frozenItems && this._frozenItems.length) {
            let reset = false;
            let absoluteIndex;
            for (let i = 0, l = this._frozenItems.length; i < l; i++) {
                absoluteIndex = this._frozenItems[i];

                // Let's first check if at least one of the frozen items is still displayed
                if (this.absoluteStartIndex <= absoluteIndex && this.absoluteEndIndex > absoluteIndex) {
                    reset = true;
                    break;
                }
            }

            // if so, let's update the list
            if (reset) {
                this._setItems(this._createItems(this.dataSource, this.absoluteStartIndex, this.absoluteEndIndex));
            }
        }
    }

    _createEmptyView() {
        return "";
    }

    _computeViewPortHeight(startIndex, size) {
        let result = 0;

        // Let's sum the height of each visible item
        for (let i = startIndex; i < (startIndex + size); i++) {
            result += this._getItemHeight(i);
        }

        return result || this._defaultItemHeight * this.state.size;
    }

    _renderItem(startIndex, item, index) {
        const isHtml = typeof item === "string";
        const clazz = "list-item " + (!item ? "empty" : ""),
            key = "item" + (startIndex + index),
            refName = key;

        let result;
        if (isHtml) {
            // FIXME : there is a security hole here, we shouldn't use dangerouslySetInnerHTML
            result = <div className={clazz} dangerouslySetInnerHTML={{__html: item}} key={key} ref={refName}/>;
        } else {
            result = <div className={clazz} key={key} ref={refName}>{item}</div>;
        }

        return result;
    }

    render() {
        return (
            <div className="list" ref="viewport" style={{height: this._computeViewPortHeight(this.state.startIndex, this.state.size)}}>
                <div className="list-container" ref="container" style={{top: -(this._getItemHeight(this.absoluteStartIndex) || this._defaultItemHeight)}}>
                    {
                        this.state.items.map((item, index) => {
                            return this._renderItem(this.absoluteStartIndex, item, index);
                        })
                    }
                </div>
            </div>
        );
    }
}


List.propTypes = {
    dataSource: React.PropTypes.array,
    size: React.PropTypes.number
};
