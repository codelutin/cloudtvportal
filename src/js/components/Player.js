/*
 * Cloud TV Portal, (C) 2015 Code Lutin (SAS).
 * Copyright (C) <year>  <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import {Component} from "./Component";
import {EventEmitter} from "../utils/EventEmitter";

export class Player extends Component {

    constructor() {
        super();
        this.topic = new EventEmitter();
        this.supportedProps = [];
    }

    static get ABORT_EVENT() { return "abort"; }
    static get ERROR_EVENT() { return "error"; }
    static get END_EVENT() { return "end"; }
    static get PLAY_EVENT() { return "play"; }
    static get PAUSE_EVENT() { return "pause"; }

    // Let's bind topic methods directly on the component
    on(...args) { this.topic.on(...args); }
    once(...args) { this.topic.once(...args); }
    emit(...args) { this.topic.emit(...args); }
    clear(...args) { this.topic.clear(...args); }

    // To be redefined in subclasses
    load() {}
    play() {}
    pause() {}
    stop() {}
    release() {}
}
