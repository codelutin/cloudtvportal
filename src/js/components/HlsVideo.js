/*
 * Cloud TV Portal, (C) 2015 Code Lutin (SAS).
 * Copyright (C) <year>  <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
const Hls = require("hls.js");

export class HlsVideo extends HTMLVideoElement {

    get CORS_URL() { return global.CORS_URL; }

    createdCallback() {
        this._isNativeHlsSupport = this.canPlayType("application/vnd.apple.mpegURL") || this.canPlayType("application/x-mpegURL");
    }

    detachedCallback() {
        this._cleanUp();
        this._hls && this._hls.destroy();
    }

    get source() {
        return this.src;
    }

    set source(url) {

        if (!this._isNativeHlsSupport
            && url && url.includes(".m3u8")
            && Hls.isSupported()) {

            this._cleanUp();
            this._loadStream(url);

        } else {
            this.src = url;
        }
    }

    _cleanUp() {
        this._hls && this._hls.destroy();
    }

    _loadStream(url, retryOnError) {
        const hls = this._hls = new Hls();

        // bind hls library to the video tag
        hls.attachMedia(this);
        hls.on(Hls.Events.MEDIA_ATTACHED, () => {
            hls.loadSource(url);
        });

        hls.on(Hls.Events.ERROR, (event, data) => {
            const fatal = data.fatal;
            const retryWithCors = !retryOnError;

            // Let's dispatch an dom error event on the player if the error is fatal
            if (fatal) {
                const e = new Event("error");
                e.retry = retryWithCors;
                this.dispatchEvent(e);
            }

            switch (data.type) {
            case Hls.ErrorTypes.NETWORK_ERROR:
                if (retryWithCors) {
                    // let's retry once with cors
                    hls.destroy();
                    this._loadStream(this.CORS_URL + url, true);

                } else if (fatal) {
                    // try to recover network error
                    hls.startLoad();
                }

                break;
            case Hls.ErrorTypes.MEDIA_ERROR:
                fatal && hls.recoverMediaError();
                break;
            default:
                // cannot recover
                fatal && hls.destroy();
                break;
            }
        });

        return hls;
    }
}
