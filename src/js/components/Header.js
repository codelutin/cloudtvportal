/*
 * Cloud TV Portal, (C) 2015 Code Lutin (SAS).
 * Copyright (C) <year>  <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import React from"react";

import {Component} from "./Component";
import {timeout} from "../utils/Timeout";
import {contentStore} from "../stores/ContentStore";

export class Header extends Component {

    constructor() {
        super();
        this.state = {};
        this._onChangeCb = (content) => this.setState(content);
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextState !== this.state;
    }

    didUpdate() {
        if (!this.props.silent && (this.state.name || this.state.description)) {
            this.show();
        }
    }

    willUnmount() {
        contentStore.clear(contentStore.CHANGE_EVENT, this._onChangeCb);
    }

    /**
     * a meda object can be delivered in props for header initialisation purpose
     */
    willMount() {
        this.state = this.props.media || {};
    }

    didMount() {
        contentStore.on(contentStore.CHANGE_EVENT, this._onChangeCb);
    }

    get HEADER_VISIBILITY_DELAY() { return 5 * 1000; }

    show(always) {
        if (!this.isEmpty()) {
            this._visible = true;
            this.nodeRoot && this.nodeRoot.classList.remove("hide");

            if (!always) {
                timeout(this.HEADER_VISIBILITY_DELAY)
                .then(() => this.hide());
            }
        }
    }

    hide() {
        this._visible = false;
        this.nodeRoot && this.nodeRoot.classList.add("hide");
    }

    toggle() {
        if (this._visible) {
            this.hide();
        } else {
            this.show();
        }
    }

    isVisible() {
        return this._visible;
    }

    isEmpty() {
        return !(this.state.logo || this.state.name || this.state.description);
    }

    render() {
        const logoStyle = {
            backgroundImage: this.state.logo ? "url('" + this.state.logo + "')" : "none"
        };
        return (
            <div className="header hide">
                <div className="header-logo">
                    <div className="header-logo-image" style={logoStyle} />
                    <div className="header-logo-name">{this.state.name}</div>
                </div>
                <div className="header-description">{this.state.description}</div>
            </div>
        );
    }
}

Header.propTypes = {
    media: React.PropTypes.object,
    silent: React.PropTypes.bool
};
