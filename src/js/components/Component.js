/*
 * Cloud TV Portal, (C) 2015 Code Lutin (SAS).
 * Copyright (C) <year>  <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import React from "react";
import ReactDOM from "react-dom";

export class Component extends React.Component {

    componentWillMount(...args) {
        this.willMount && this.willMount(...args);
    }

    componentDidMount(...args) {
        this._nodeRoot = ReactDOM.findDOMNode(this);
        const eventsHandler = this.context.eventsHandler || this.props.eventsHandler;
        eventsHandler && eventsHandler.bindEvents(this, this._nodeRoot);
        this.didMount && this.didMount(...args);
    }

    componentWillReceiveProps(...args) {
        this.willReceiveProps && this.willReceiveProps(...args);
    }

    componentWillUpdate(...args) {
        this.willUpdate && this.willUpdate(...args);
    }

    componentDidUpdate(...args) {
        this._nodeRoot = ReactDOM.findDOMNode(this);
        this.didUpdate && this.didUpdate(...args);
    }

    componentWillUnmount(...args) {
        this._nodeRoot = null;
        this.willUnmount && this.willUnmount(...args);
    }

    get nodeRoot() {
        return this._nodeRoot;
    }

}

Component.contextTypes = {
    api: React.PropTypes.object,
    eventsHandler: React.PropTypes.object
};

Component.propTypes = {
    eventsHandler: React.PropTypes.object
};
