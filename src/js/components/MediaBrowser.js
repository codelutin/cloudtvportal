/*
 * Cloud TV Portal, (C) 2015 Code Lutin (SAS).
 * Copyright (C) <year>  <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import React from"react";

import {Component} from "./Component";
import {Player} from "./Player";
import {playerFactory} from "../utils/PlayerFactory";
import {ContentActionCreator} from "../actions/ContentActionCreator";

/**
 * The MediaBrowser play a list of media.
 * It relies on a dataSource. The dataSource is either an array of media objects or a DataDource Source object ({next():Promise})
 *
 * A media object is an object exposing the following attributes:
 * - *type: String
 * - *url: String
 * - title: String
 * - logo: String
 *
 * attribute with *: mandatory
 */
export class MediaBrowser extends Component {

    constructor() {
        super();
        this.state = {
            media: null
        };
    }

    didMount() {
        this.next();
    }

    get dataSource() {
        if (!this._dataSource) {
            this._dataSource = this.props.dataSource;
            if (Array.isArray(this._dataSource)) {
                this._dataSource = MediaBrowser.dataSourceFromArray(this._dataSource);
            }
        }
        return this._dataSource;
    }

    set dataSource(dataSource) {
        this._dataSource = dataSource;
    }

    static dataSourceFromArray(values) {
        return {
            next() {
                let result;
                this.crt = this.crt != null ? this.crt : 0;
                if (this.crt < values.length) {
                    result = values[this.crt++];
                }

                return result;
            }
        };
    }

    next() {
        let result = Promise.resolve();

        const dataSource = this.dataSource;
        if (dataSource) {
            result = dataSource.next();
            result = result && result.then ? result : Promise.resolve(result);
            result.then((media) => {
                this.setState({media, message: "Loading " + (media.title || "") + "..."}); // FIXME : should be translated trough i18n

                // Alert component following content state that the media content has been updated
                this.refs.player.once(Player.PLAY_EVENT, () => {
                    ContentActionCreator.updateContent(media.logo, !media.logo ? media.type : "", media.title || media.url, media.url);
                    this.setMessage("");
                });

                this.refs.player.once(Player.ERROR_EVENT, (e) => {
                    const mess = "Sorry, an error occured" + (e.retry ? ", retrying..." : ""); // FIXME : should be translated trough i18n
                    this.setMessage(mess);
                });

                // Play next media
                this.refs.player.once(Player.END_EVENT, () => {
                    this.next();
                });
            });
        }

        return result;
    }

    setMessage(message) {
        const messageNode = this.refs.message;
        if (messageNode) {
            messageNode.innerHTML = message; // FIXME : should be updated through setState
        }
    }

    _renderPlayer(media) {
        let result;
        if (media) {
            const PlayerClazz = playerFactory.get(media.type);

            // We're using a new unique key each time we're creating a player to be sure a new player node will be generated
            // (if the datasource contains twice the same url, following each other, then we want the video to be played twice => the url cn't be used here as a key then)
            result = <PlayerClazz autoPlay key={this.context.api.nonce()} ref="player" src={media.url}/>;
        }

        return result;
    }

    render() {
        return (
            <div className="mediaBrowser">
                {this._renderPlayer(this.state.media)}
                <div className="mediaBrowser-message" ref="message">{this.state.message}</div>
            </div>
        );
    }
}

MediaBrowser.propTypes = {
    dataSource: React.PropTypes.any
};
