/*
 * Cloud TV Portal, (C) 2015 Code Lutin (SAS).
 * Copyright (C) <year>  <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import React from "react";

import {Component} from "./Component";

import {logger} from "../utils/Logger";
import {Animation} from "../utils/animation/Animation";
import {AnimationKeyFrame} from "../utils/animation/AnimationKeyFrame";

/**
 * This component will help you transition between two screen state, keeping track of the previous states
 *
 * To use it, wrap your components into a Transitioner :
 * <Transitioner>
 *     <MyComponent />
 *     <MyOtherComponent>
 *         <MyChildComponent/>
 *     </MyOtherComponent>
 * </Transitioner>
 *
 * From one of you component, call :
 * this.context.eventsHandler.bubble(this.nodeRoot, "open", <MyThirdComponent />, "fade");
 *
 * this will replace all the wrapped components by <MyThirdComponent/> with a fade anim.
 * If you want to go back to the previous state, just call :
 * this.context.eventsHandler.bubble(this.nodeRoot, "close");
 */
export class Transitioner extends Component {

    constructor() {
        super();
        this._defaultAnimationName = "none";
        this._defaultAnimationOptions = {fill: "forwards", duration: 500};

        this._contentKeys = new Map();

        this.transitioners = new Map();
        this.transitioners.set("none", [this.noAnim, this.noAnim]);
        this.transitioners.set("fadeIn", [this.fadeIn, this.hide]);
        this.transitioners.set("fadeOut", [this.show, this.fadeOut]);
        this.transitioners.set("fadeInOut", [this.fadeIn, this.fadeOut]);

        this.state = {
            animationName: null,
            contentIn: null,
            contentOut: null
        };

        this._history = [];
    }

    didMount() {
        // Let's register public events
        const eh = this.context.eventsHandler;
        eh.bindEvent("open", this, this.nodeRoot);
        eh.bindEvent("close", this, this.nodeRoot);
    }

    get noAnim() {
        return {
            apply: () => Promise.resolve()
        };
    }

    get fadeIn() {
        const keyframes = [new AnimationKeyFrame().opacity(0), new AnimationKeyFrame().opacity(1)];
        return new Animation(keyframes, this._defaultAnimationOptions);
    }

    get fadeOut() {
        const keyframes = [new AnimationKeyFrame().opacity(1), new AnimationKeyFrame().opacity(0)];
        return new Animation(keyframes, this._defaultAnimationOptions);
    }

    get hide() {
        return new AnimationKeyFrame().opacity(0);
    }

    get show() {
        return new AnimationKeyFrame().opacity(1);
    }

    getAnimations(animationName) {
        return this.transitioners.get(animationName || this._defaultAnimationName);
    }

    open(element, animationName) {
        const state = {
            animationName,
            contentIn: element,
            contentOut: this.state.contentIn || this.props.children
        };

        this._history.push(state);

        return this._applyTransition(state, this.props.silent);
    }

    close() {
        let result = Promise.resolve();

        if (this._history.length) {
            const prevState = this._history.pop();
            result = this._applyTransition({
                animationName: prevState.animationName,
                contentIn: prevState.contentOut,
                contentOut: prevState.contentIn
            }, this.props.silent);
        }

        return result;
    }

    clear() {
        let result = Promise.resolve();

        if (this._history.length) {
            const initialState = this._history[0];
            this._history = [];
            result = this._applyTransition({
                animationName: this._defaultAnimationName,
                contentIn: initialState.contentOut,
                contentOut: this.state.contentIn
            }, true);
        }

        return result;
    }

    _applyTransition(state, preventFocus) {
        // Let's attach the new element into the dom
        this.setState(state);

        const [animIn, animOut] = this.getAnimations(state.animationName);
        const wrapperIn = this.refs.slot1,
            wrapperOut = this.refs.slot2;

        // Set the focus on new content
        if (!preventFocus) {
            this.context.eventsHandler.focus(wrapperIn.firstElementChild);
        }

        // Let's make the transition
        const result = Promise.all([animIn.apply(wrapperIn), animOut.apply(wrapperOut)]);
        result.then(
            () => this.setState({contentIn: state.contentIn, contentOut: null}),
            () => logger.warn("[Transitioner] - Warning : Couldn't transition between", wrapperIn, "and", wrapperOut)
        );

        return result;
    }

    didReceiveFocus() {
        const content = this.refs.slot1.firstElementChild;
        if (content) {
            this.context.eventsHandler.focus(content);
        }
        return false;
    }

    _renderSlot(refName, content) {
        let result;

        // We need to assign a key to each content in order to enable react virtual dom comparison
        // and avoid to re mount a content that is already mounted
        //
        // For example when opening a new content :
        // 1. the transitioner re render placing the new content in the "contenIn" and the current content in the "contentOut" container
        // 2. the opening transition is triggered
        // 3. the transitioner re render with only the new content (the previous one is not out)
        //
        // During the first phase we don't want the currentContent to be rerendered since it is already mounted, we just want the node to move
        // so that the new content takes its place.
        // To permit this operation, we need to give react the ability to recognize which content is loaded where through the key system.
        let key = content ? this._contentKeys.get(content) : this.context.api.nonce();
        if (!key) {
            key = this.context.api.nonce();
            this._contentKeys.set(content, key);
        }

        const isHtml = typeof content === "string";
        if (isHtml) {
            // FIXME : there is a security hole here, we shouldn't use dangerouslySetInnerHTML
            result = <div className="transitioner-slot gl" dangerouslySetInnerHTML={{__html: content}} key={key} ref={refName} />;
        } else {
            result = (
                <div className="transitioner-slot gl" key={key} ref={refName}>
                    {content}
                </div>
            );
        }

        return result;
    }

    render() {
        return (
            <div className="transitioner unfocusable">
                {this._renderSlot("slot1", this.state.contentIn || this.props.children || null)}
                {this._renderSlot("slot2", this.state.contentOut)}
            </div>
        );
    }
}

Transitioner.propTypes = {
    children: React.PropTypes.element,
    silent: React.PropTypes.bool
};
