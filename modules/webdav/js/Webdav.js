export class Webdav {

    constructor(api, moduleId) {
        this.root = {
            id: "webdav",
            moduleId: moduleId,
            type: "webdav",
            version: 0,
            view: api.theme.createSource("WebDav")
        };

        this.addServerItem = {
            id: "add" + this.root.id,
            moduleId: moduleId,
            type: "webdav",
            version: this.root.version,
            view: api.theme.createChild("Add new Webdav server"),
            onClick: () => {
                // FIXME how to write this page, and how to open it.
                return Promise.resolve({
                    type: api.TYPE_URL,
                    src: "addWebdavServer.html"
                });
            },
            path: "/"
        };

        this.api = api;
    }

    get id() {
        return this.api.getModuleId(this);
    }

    getRoot() {
        return this.root;
    }

    getData(item) {
        this.api.logger.log("retrieve data");
        if (!item.path) {
            // read configured server from storage. Each server must have:
            // name, url, login, password
            let list = this.api.syncStorage.get("webdav-server") || [];
            let result = list.map(server => {
                return {
                    id: item.type + ":" + server.url,
                    moduleId: this.id,
                    type: item.type,
                    version: item.version,
                    view: this.api.theme.createSource(server.name),
                    webdav: {
                        name: server.name,
                        login: server.login,
                        password: server.password,
                        url: server.url,
                        authorization: "Basic " + btoa(server.login + ":" + server.password),
                        getUrl: this._webdavGetUrl(server.url, server.login, server.password)
                    },
                    path: []
                };
            });

            // add possibility to add new webdav
            result.push(this.addServerItem);
            return Promise.resolve(result);
        }

        this._webdavGetChildren(item).then(children => {
            return Promise.resolve(children.map(file => {
                return {
                    id: item.type + ":" + file.url,
                    moduleId: this.id,
                    type: item.type,
                    version: item.version,
                    view: this.api.theme.createSource(file.name),
                    webdav: item.webdav,
                    path: item.path.concat(file.name), // need a copy
                    file,
                    onClick: () => Promise.resolve({
                        type: this.api.TYPE_URL,
                        src: file.url
                    })
                };
            }));
        });
    }

    getSyncViewOnCreate(item) {
        return item.view;
    }

    onClick(item) {
        let result;
        if (item.onClick) {
            result = item.onClick();
        } else if (item.url) {
            return Promise.resolve({
                type: this.api.TYPE_VIEW,
                src: <img src={item.url}/>
            });
        }
        return result;
    }


    serialize(item) {
        return JSON.stringify(item);
    }
    deserialize(string) {
        return JSON.parse(string);
    }
    accept(itemOrString) {
        let item = itemOrString;
        if (typeof itemOrString === "string") {
            item = this.deserialize(itemOrString);
        }

        return item.type === "webdav";
    }

    _webdavGetUrl(url, login, password) {
        let result = url.replace(/^((?:[a-zA-Z0-9+]+:)?\/\/)([^\/]+)(\/.*)?$/, (match, protocol, domain, path) => {
            return protocol + encodeURIComponent(login) + ":" + encodeURIComponent(password) + "@" + path;
        });
        return result;
    }

    _webdavGetChildren(item) {
        let promise = new Promise((resolve, reject) => {
            let url = item.webdav.url + item.path.join("/");

            let q = new XMLHttpRequest();
            q.open("PROPFIND", url, true);
            q.setRequestHeader("Depth", 1);
            q.setRequestHeader("Content-type", "text/xml; charset=UTF-8");
            q.setRequestHeader("Authorization", item.webdav.authorization);
            q.onreadystatechange = () => {
                if (q.readyState === 4) {
                    if (q.status < 300) {
                        let result = this._parse(item, url, q.responseText);
                        resolve(result);
                    } else {
                        reject();
                    }
                }
            };
            q.send();
        });
        return promise;

    }

    _parse(url, xml) {
        let result = [];

        let urlGet = this.urlGet;
        let prefixLength = url.replace(/^((?:[a-zA-Z0-9+]+:)?\/\/)([^\/]+)(\/.*)?$/, "$3").length;

        let parser = new this.api.XMLParser();
        parser.saxParser.ns("dav", { // or false
            "DAV:": "dav"
        });

        parser.on("dav:multistatus", function() {
            parser.on("dav:response", function() {
                let file = {};
                parser.on("dav:href/$content", function(text) {
                    file.url = urlGet + text;

                    if (text[text.length - 1] === "/") {
                        text = text.substring(prefixLength, text.length - 1);
                    } else {
                        text = text.substring(prefixLength, text.length);
                    }

                    if (text[0] === "/") {
                        text = text.substring(1);
                    }

                    if (text) {
                        text = decodeURIComponent(text);
                        text.replace(/^.+\.(.*)?$/, (match, ext) => {file.extension = ext;});
                        file.name = text;
                        result.push(file);
                    }
                });
                parser.on("dav:propstat/dav:prop/dav:getlastmodified/$content", function(t) {
                    file.lastModified = new Date(t);
                });
                parser.on("dav:propstat/dav:prop/dav:getcontenttype/$content", function(t) {
                    file.mimeType = t;
                });
                parser.on("dav:propstat/dav:prop/dav:getcontentlength/$content", function(t) {
                    file.size = parseInt(t, 10);
                });
                parser.on("dav:propstat/dav:prop/dav:resourcetype/dav:collection/$content", function() {
                    file.isFolder = true;
                });
            });
        });

        parser.write(xml);

        return result;
    }

}
