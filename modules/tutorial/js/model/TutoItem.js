/*
 * Cloud TV Portal, (C) 2015 Code Lutin (SAS).
 * Copyright (C) <year>  <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import {TutoItemDescription} from "./TutoItemDescription";

export class TutoItem {
    constructor(moduleId, name, items, description, icon) {
        this.moduleId = moduleId;
        this.name = name;
        this.items = items;
        this.description = description || {};
        this.icon = icon;

        this.type = "tuto";

        if (description) {
            this.description = new TutoItemDescription(moduleId, description, icon);
        }

        this.style = {
            width: 220
        };
    }

    getItems() {
        return this.items;
    }

    onClick(api) {
        return Promise.resolve({
            type: api.TYPE_URL,
            src: "https://en.wikipedia.org/wiki/" + this.name
        });
    }

    onFocus(api) {
        if (this.description) {
            return new Promise((resolve) => {
                setTimeout(() => resolve({
                    type: api.TYPE_VIEW,
                    src: {
                        value: api.theme.createSimpleContent(this.description.text, this.description.icon, this.description.style)
                    },
                    isSilent: true
                }), 1000 * Math.random()); // Simulate a random response time before action
            });
        }
    }
}
