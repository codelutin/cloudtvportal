/*
 * Cloud TV Portal, (C) 2015 Code Lutin (SAS).
 * Copyright (C) <year>  <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import {TutoItem} from "./model/TutoItem";

export class Tutorial {
    constructor(api) {
        this.api = api;
    }

    get id() {
        return this.api.getModuleId(this);
    }

    getRoot() {
        const root = {
            id: "tutorial",
            isRoot: true,
            moduleId: this.id,
            type: "tuto",
            view: this.api.theme.createSource("Tutorial")
        };

        const fruits = new TutoItem(this.id, "Fruits",
            [
                new TutoItem(this.id, "Apple", null,
                    "The apple tree is a deciduous tree in the rose family best known for its sweet, pomaceous fruit, the apple." +
                    " It is cultivated worldwide as a fruit tree, and is the most widely grown species in the genus Malus.",
                    "img/apple.jpg"
                ),
                new TutoItem(this.id, "Banana", null,
                    "The banana is an edible fruit, botanically a berry, produced by several kinds of large herbaceous" +
                    "flowering plants in the genus Musa. " +
                    "In some countries, bananas used for cooking may be called plantains",
                    "img/banana.jpg"
                ),
                new TutoItem(this.id, "Orange"),
                new TutoItem(this.id, "Kiwi", null,
                    "The kiwifruit or Chinese gooseberry is the edible berry of a woody vine in the genus Actinidia. " +
                    "The most common cultivar group of kiwifruit is oval, about the size of a large hen's egg"
                ),
                new TutoItem(this.id, "Grappes")
            ],
            null,
            "img/fruits.jpg"
        );

        const vegetables = new TutoItem(this.id, "Vegetables",
            [
                new TutoItem(this.id, "Kale", null,
                    "Kale or leaf cabbage is a group of vegetable cultivars within the plant species Brassica oleracea. " +
                    "They have green or purple leaves, in which the central leaves do not form a head.",
                    "img/kale.jpg"
                ),
                new TutoItem(this.id, "Cucumber", null,
                    "Cucumber is a widely cultivated plant in the gourd family, Cucurbitaceae. " +
                    "It is a creeping vine that bears cylindrical fruits that are used as culinary vegetables. " +
                    "There are three main varieties of cucumber: slicing, pickling, and burpless.",
                    "img/cucumber.jpg"
                ),
                new TutoItem(this.id, "Celery", null,
                    "Celery is a cultivated plant, variety in the family Apiaceae, commonly used as a vegetable. " +
                    "Depending on location and cultivar, either its stalks or its hypocotyl are eaten and used in cooking.",
                    "img/celery.jpg"
                ),
                new TutoItem(this.id, "Leek", null,
                    "The leek is a vegetable that belongs, along with onion and garlic, to the genus Allium, " +
                    "currently placed in family Amaryllidaceae, subfamily Allioideae. " +
                    "Historically, many scientific names were used for leeks, which are now treated as cultivars of Allium ampeloprasum. " +
                    "The name 'leak' developed from the Anglo-Saxon word leac. " +
                    "Two related vegetables, elephant garlic and Kurrat, are also cultivars of A. ampeloprasum, " +
                    "although different in their uses as food.",
                    "img/leek.jpg"
                ),
                new TutoItem(this.id, "Broccoli", null,
                    "Broccoli is an edible green plant in the cabbage family whose large flower-head is eaten as a vegetable.",
                    "img/broccoli.jpg"
                ),
                new TutoItem(this.id, "Cauliflower"),
                new TutoItem(this.id, "Lettuce"),
                new TutoItem(this.id, "Zucchini"),
                new TutoItem(this.id, "Spinach"),
                new TutoItem(this.id, "Pepper", null, "img/pepper.jpg"),
                new TutoItem(this.id, "Sweet potato"),
                new TutoItem(this.id, "Garlic"),
                new TutoItem(this.id, "Onion"),
                new TutoItem(this.id, "Tomato", null, "img/tomato.jpg")
            ],
            null,
            "img/vegetables.jpg"
        );

        root.getItems = () => {
            return [fruits, vegetables];
        };

        return root;
    }

    getData(item) {
        return Promise.resolve(item && item.getItems && item.getItems());
    }

    getSyncViewOnCreate(item) {
        return item.view || this.api.theme.createChild(item.name, null, item.style);
    }
    getSyncViewOnFocus(item) {
        return !item.isRoot && this.api.theme.createChild(item.name, item.icon && item.icon.replace(".", "_focused."), item.style);
    }
    getAsyncViewOnCreate(item) {
        return new Promise((resolve, reject) => {
            if (item.isRoot || item.view) {
                // don't update an item which already have a view. (this is just for demo)
                reject();
            } else {
                setTimeout(
                    () => {
                        item.view = this.api.theme.createChild(item.name, item.icon, item.style);
                        resolve(item.view);
                    },
                    1000 * Math.random()
                );
            }
        });
    }
    getAsyncViewOnFocus(item) {
        return new Promise((resolve, reject) => {
            if (item.isRoot) {
                reject();
            } else {
                setTimeout(
                    () => {
                        const subItemsCount = (item.getItems() || []).length;
                        resolve(this.api.theme.createChild(item.name + " (" + subItemsCount + ")", item.icon && item.icon.replace(".", "_focused."), item.style));
                    },
                    1000 * Math.random()
                );
            }
        });
    }

    onFocus(item) { return item && item.onFocus && item.onFocus(this.api); }
    onClick(item) { return item && item.onClick && item.onClick(this.api); }

    serialize(item) {
        return JSON.stringify(item);
    }
    deserialize(string) {
        return JSON.parse(string);
    }
    accept(itemOrString) {
        let item = itemOrString;
        if (typeof itemOrString === "string") {
            item = this.deserialize(itemOrString);
        }

        return item && item.type === "tuto";
    }
}
