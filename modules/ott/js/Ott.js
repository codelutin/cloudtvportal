/*
 * Cloud TV Portal, (C) 2015 Code Lutin (SAS).
 * Copyright (C) <year>  <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
export class Ott {
    constructor(api) {
        this.api = api;
    }

    get id() {
        return this.api.getModuleId(this);
    }

    getRoot() {
        const root = {
            id: "ott",
            moduleId: this.id,
            view: this.api.theme.createSource("Ott")
        };

        root.getItems = () => {
            return [
                this._createEntry("Adult Swim", "img/adultswim.png", "http://adultswimhls-i.akamaihd.net/hls/live/238460/adultswim/main/1/master_Layer5.m3u8"),
                this._createEntry("Blender", "img/blender.png",
                    "https://archive.org/download/Sintel.2010.1080p/Sintel.2010.1080p.mp4",
                    "https://archive.org/download/BigBuckBunny_328/BigBuckBunny_512kb.mp4",
                    "https://archive.org/download/Caminandes1LlamaDrama/01_llama_drama_1080p.mp4",
                    "https://archive.org/download/Caminandes2GranDillama/02_gran_dillama_1080p.mp4"
                ),
                this._createEntry("Fox News", "img/foxnews.png", "http://cdn3.videos.bloomberg.com/btv/us/master.m3u8"),
                this._createEntry("iConcerts", "img/iconcerts.png", "http://iconcerts-channel.videocdn.scaleengine.net/iconcerts-channel/live/iconcerts-livetv/playlist.m3u8"),
                this._createEntry("Cartoons", "img/archive.png",
                    "https://archive.org/download/haul_in_one/haul_in_one_512kb.mp4",
                    "https://archive.org/download/Popeye_forPresident/Popeye_forPresident_512kb.mp4",
                    "https://archive.org/download/popeye_patriotic_popeye/popeye_patriotic_popeye_512kb.mp4",
                    "https://archive.org/download/popeye_private_eye_popeye/popeye_private_eye_popeye_512kb.mp4",
                    "https://archive.org/download/superman_1941/superman_1941_512kb.mp4",
                    "https://archive.org/download/SpaceTransformer/SpaceTransformer_512kb.mp4"
                ),
                this._createEntry("Streamer House", "img/twitch.png",
                    "http://video-edge-49b358.cdg01.hls.ttvnw.net/hls66/streamerhouse_20919334768_439824956/chunked/index-live.m3u8?token=id=6541209316227054173,bid=20919334768,exp=1461328754,node=video-edge-49b358-1.cdg01.hls.justin.tv,nname=video-edge-49b358.cdg01,fmt=chunked&sig=ff993442694170a64d17339ca2d748a2a97524fc"
                )
            ];
        };

        return root;
    }

    _createEntry(name, icon, ...vidUrls) {
        const style = {width: 300};

        let sources = [];
        for (let i = 0, l = vidUrls.length; i < l; i++) {
            sources.push({url: vidUrls[i], type: "video", logo: icon, title: name});
        }

        return {
            moduleId: this.id,
            name,
            icon,
            style,
            view: this.api.theme.createChild(name, icon, style),
            onClick: () => Promise.resolve({
                type: this.api.TYPE_MEDIA,
                src: sources
            })
        };
    }

    getData(item) {
        return Promise.resolve(item && item.getItems && item.getItems());
    }

    getSyncViewOnCreate(item) {
        return item.view;
    }
    // getSyncViewOnFocus() {}
    // getAsyncViewOnCreate() {}
    // getAsyncViewOnFocus() {}

    onFocus() {}
    onClick(item) {
        return item.onClick();
    }

    serialize(item) {
        return JSON.stringify(item);
    }
    deserialize(string) {
        return JSON.parse(string);
    }
    accept(itemOrString) {
        let item = itemOrString;
        if (typeof itemOrString === "string") {
            item = this.deserialize(itemOrString);
        }

        return item.moduleId === this.id;
    }
}
