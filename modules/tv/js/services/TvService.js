/*
 * Cloud TV Portal, (C) 2015 Code Lutin (SAS).
 * Copyright (C) <year>  <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

export class TvService {

    constructor() {
        const oipf = window.__oipf__;
        this.dataProvider = oipf && new oipf.JsonTvProvider({
            maxDate: Date.now() + 3600 * 1000 * 4, // 4 hours after now
            endpoint: "//proxy-xmltv.nuiton.org/tnt.json"
            // endpoint: "//192.168.99.124:9080/tnt.json"
        });
    }

    _getOipfObjectFactory() {
        return window.oipfObjectFactory;
    }

    /**
     * Retrive oipf channel list and return a list of Item
     */
    getChannels() {
        let vb = this._getOipfObjectFactory().createVideoBroadcastObject(),
            cc = vb.getChannelConfig();

        return cc.channelList;
    }

    /**
     * Retrive oipf programmes corresponding to a channel
     */
    getProgrammes(channel) {
        return new Promise((resolve) => {

            const searchMetadataObject = this._getOipfObjectFactory().createSearchManagerObject();
            searchMetadataObject.onMetadataSearch = (result) => {
                this._enhanceResult(channel, result.result)
                .then(
                    (r) => resolve(r),
                    (r) => resolve(r)
                );
            };
            const search = searchMetadataObject && searchMetadataObject.createSearch(1);
            const query = search.createQuery("Programme.startTime", 2, Date.now() / 1000);

            search.setQuery(query);
            search.addChannelConstraint(channel);
            search.result.getResults(0, 10);
        });
    }

    _enhanceResult(channel, programmes) {
        let result = Promise.resolve(programmes);

        if (this.dataProvider) {
            result = this.dataProvider.getProgrammes(0, channel)
            .then((progs) => {
                const progsMap = new Map();
                const getProgKey = (prog) => prog.startTime + "-" + prog.name.replace(" ", "").substr(0, 10);

                let prog;
                for (let i = 0, l = progs.length; i < l; i++) {
                    prog = progs[i];
                    progsMap.set(getProgKey(prog), prog);
                }

                let matchingProg;
                for (let i = 0, l = programmes.length; i < l; i++) {
                    prog = programmes[i];
                    matchingProg = progsMap.get(getProgKey(prog));

                    if (matchingProg) {
                        prog.image = matchingProg._logoURL;
                    }
                }

                return programmes;
            }, () => programmes);
        }

        return result;
    }
}
