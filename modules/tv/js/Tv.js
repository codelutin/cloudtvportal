/*
 * Cloud TV Portal, (C) 2015 Code Lutin (SAS).
 * Copyright (C) <year>  <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import {TvService} from "./services/TvService";

import React from "react";

export class Tv {

    get AVAILABLE_OTT_CHANNELS() {
        return {
            6: "https://sslhls.m6tv.cdn.sfr.net/hls-live/livepkgr/_definst_/m6_hls_aes/m6_hls_aes_856.m3u8",
            7: "http://frlive.artestras.cshls.lldns.net/artestras/contrib/frlive/frlive_925.m3u8",
            9: "https://sslhls.m6tv.cdn.sfr.net/hls-live/livepkgr/_definst_/w9_hls_aes/w9_hls_aes_856.m3u8",
            4133: "https://sslhls.m6tv.cdn.sfr.net/hls-live/livepkgr/_definst_/six_ter_hls_aes/six_ter_hls_aes_856.m3u8"
        };
    }

    constructor(api) {
        this.api = api;
        this.provider = new TvService();
    }

    get id() {
        return this.api.getModuleId(this);
    }

    getRoot() {
        const root = {
            moduleId: this.id,
            view: this.api.theme.createSource("TV"),
            getItems: () => {
                const result = [];
                const channels = this.provider.getChannels();

                // Wrap channels in Item model object
                for (let i = 0, l = channels.length; i < l; i++) {
                    result.push(this._createItemFromChannel(channels[i]));
                }

                return result;
            }
        };

        return root;
    }

    getData(item) {
        let result;

        if (!item) {
            let err = "[Tv Service] - getData : missing item param";
            this.api.logger.error(err);
            result = Promise.reject(err);
        } else {
            result = item.getItems && item.getItems();
            if (Array.isArray(result)) {
                result = Promise.resolve(result);
            }
        }

        return result;
    }

    getSyncViewOnCreate(item) {
        return item.view;
    }

    onFocus() {}
    onClick(item) {
        return item.onClick && item.onClick();
    }

    serialize(item) {
        return JSON.stringify(item);
    }
    deserialize(string) {
        return JSON.parse(string);
    }
    accept(itemOrString) {
        let item = itemOrString;
        if (typeof itemOrString === "string") {
            item = this.deserialize(itemOrString);
        }

        return item.moduleId === this.id;
    }

    _createItemFromChannel(channel) {
        const style = {width: 300, visibleItemsSize: 4},
            channelLogoUrl = "img/" + channel.majorChannel + ".png";
        return {
            id: channel.id,
            moduleId: this.id,
            style,
            view: this.api.theme.createChild(channel.name, channelLogoUrl, style),
            getItems: () => {
                return new Promise((resolve) => {
                    this.provider.getProgrammes(channel)
                    .then((programmes) => {
                        const result = [];

                        // Wrap channels in Item model object
                        for (let i = 0, l = programmes.length; i < l; i++) {
                            result.push(this._createItemFromProgramme(programmes[i]));
                        }

                        resolve(result);
                    });
                });
            },
            onClick: () => {
                let result;
                const ottUrl = this.AVAILABLE_OTT_CHANNELS[channel.majorChannel];
                if (ottUrl) {
                    result = Promise.resolve({
                        type: this.api.TYPE_MEDIA,
                        src: [{url: ottUrl, type: "video", logo: channelLogoUrl, title: channel.name}]
                    });
                } else {
                    result = Promise.resolve({
                        type: this.api.TYPE_VIEW,
                        src: {
                            value: "<div class='fit' style='background: url(" + channelLogoUrl + ") center no-repeat / contain'></div>",
                            title: channel.name,
                            logo: channelLogoUrl
                        }
                    });
                }
                return result;
            }
        };
    }

    _createItemFromProgramme(programme) {
        const style = {height: 235, width: 350};

        return {
            id: programme.id,
            moduleId: this.id,
            style,
            view: this._createProgrammeTile(programme, style),
            getItems: () => {
                return Promise.resolve([
                    this._createProgrammeActionItem("Record", "img/record.png"),
                    this._createProgrammeActionItem("Watch in EPG", "img/epg.png")
                ]);
            }
        };
    }

    _createProgrammeTile(programme, style) {
        return (
            <div className="tv-programmeTile" style={style}>
                <img className="tv-programmeTile-image" src={programme.image || "//:0"} />
                <div className="tv-programmeTile-text">
                    <span className="tv-programmeTile-startTime">{this.api.moment.unix(programme.startTime).format("HH:mm")} - </span>
                    <span className="tv-programmeTile-name">{programme.name}</span>
                    <div className="tv-programmeTile-desc">{programme.description}</div>
                </div>
            </div>
        );
    }

    _createProgrammeActionItem(name, img) {
        const style = {width: 220};
        return {
            moduleId: this.id,
            style,
            view: this.api.theme.createChild(name, img, style, {isSmall: true})
        };
    }
}
